######
Hiring
######

New core memebers in a paid capacity is a great way to expand our team,
abilities, passion, bandwidth and culture.
In this page you can find information about the hiring process and how we do it
a bit differently in LSF.

*******
General
*******

In LSF, recruitment is led by the team in need of a new member (specifically
the Project Manager), not by Human Resources team (which only takes care of the
practicalities).
Conversations with candidates tend to center around three topics: Fit with the
role, fit with the organization, and fit with the purpose.
The last two are often considered more important, as in self-managing
organizations like LSF, there is much fluidity around roles.
Thus a prioritized list of topics is as follows:

1. Fit with purpose
2. Fit with organization
3. Briefing about needs
4. Drafting roles covering needs

A fundamental question is asked for any prospective new core member in paid
capacity:

- **Do we sense that we are meant to journey together?**

This question can only be meaningfully answered when conversations are rooted
in honesty and integrity, with a willingness to inquire deeply and openly.

*****
Roles
*****

Each Project Manager with their respective peers, defines roles (see
:doc:`../projects/roles`) for their projects in a regular cadence (some
projects Quarterly others Annually etc).

Every month a dedicated team meets to define priorities for the vacant roles
identified per project.
The criteria for prioritization are project urgency, timelines, financial
viability and other organization considerations.

This list is published in our website (LINK TO BE ADDED HERE) and can be seen
by anyone interested.

.. note::
    **Roles and NOT Positions!**

    LSF uses roles and does not follow the traditional position definition
    practice for recruiting/hiring.
    This essentially means that prospective candidates will be applying to join
    a single or multiple teams, in a single or multiple roles.
    LSF actively encourages new candidates to propose *their own roles too*,
    based on their expertise, passion and ideas that fit our mission and
    vision.



*******
Process
*******

1. Prospective applicant reviews the list of (vacant) roles (LINK NEEDED HERE).
2. Applicant reaches out to project managers as stated in our
   :doc:`../projects/projects-portfolio`, to discuss general fit for the role.
3. Applicant sends an email to hr@libre.space with 4 key information:

    a. Roles interested in and how the applicant see themselves fit in them
       (experience, motivations etc)
    b. Applicant self-assessment for Level (see
       :doc:`../people/paid-contributors`)
    c. Applicant self-assessment for cultural fit in the organization
    d. Any additional ideas/roles the applicant would like to bring forward

4. HR team opens a tracking issue in hr-org repo and a Hiring Manager is
   assigned for cultural fit interview.
5. After the cultural fit interview, the issue is updated with notes and the
   applicant is directed (or not) to further interviews as needed with project
   managers or prospective peers.
6. After each interview the issue is updated with notes.
7. The Hiring Manager ensures that an offer can be made if the following are
   all true:

    a. Is the candidate a good organization fit?
    b. Is the candidate a good fit for the roles applied?
    c. Is the candidate a good fit for our organization purpose?
    d. Is there available budget from the Accounting team for the level
       specified?

8. An offer is made to the candidate by the Hiring Manager.
9. If the offer is accepted, then onboarding process begins (see
   :doc:`../people/core-contributors`).

***********
A good fit?
***********

.. note::
   Many of the following information and LSF hiring practices can be found in
   `Reinventing Organizations Wiki
   <https://reinventingorganizationswiki.com/en/theory/recruitment/>`_.

Fit for role
============

Assessing the fit in terms of skills, experience and expertise remains an
important component of the recruitment process, especially for specific roles
requiring expertise.
Roles in self-managing organizations are exchanged very fluidly, though.
For that reason, the "fit for role" is often not considered to be paramount, as
it is likely that a person's roles might change quickly.
Self-managing organizations experience that when employees are motivated to
take on a new and challenging role, they pick up new skills and experience in
surprisingly little time.

Fit with the organization
=========================

A second area to explore in conversation is: will this person blossom in the
organization?
Will he or she thrive in a self-organizing environment?
Does the person feel aligned by the organization's values?
Does he or she "click" with the colleagues?

Fit with purpose
================

.. note::
   Our `Manifesto <https://manifesto.libre.space>`_ can act as a pefect guide
   for purpose 🚀 !

Finally, is the person energized by the organizations’ purpose?
Is there something in the person's history that makes them resonate, makes them
want to serve this purpose at this moment in their life?
The discussion triggered by these questions can reach substantial depth and
help both the candidate and the organization learn more about themselves.
Recruitment becomes a process of self-inquiry as much as a process of mutual
assessment.
