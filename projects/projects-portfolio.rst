##################
Projects Portfolio
##################

.. contents::
   :local:
   :backlinks: none

.. _Pierros Papadeas: https://gitlab.com/pierros

********
Overview
********

Libre Space Foundation develops and supports a variety of open-source projects
for space.
For each project there is a project manager and a project champion (more on
those :doc:`roles`).

*************************
Currently active projects
*************************

The following major projects are in active development:

.. gitlabissues::
   :project: librespacefoundation/lsf-core
   :labels: Ongoing
   :sections_regex: ^\#{2}\s*(.*?)(?:\#{2})?\s*\n+(.*?)\n*(?=\n\#{2}|\Z)

   .. list-table::
      :header-rows: 1

      * - Project
        - Manager
        - Champion
   {%- for item in issues | rejectattr('confidential') -%}
   {%- if 'Administrative' not in item.labels -%}
   {%- if item.content['Public link'] is defined and item.content['Public link'].startswith('http') %}
      * - `{{ item.title }} <{{ item.content['Public link'].splitlines() | first }}>`_
   {%- else %}
      * - {{ item.title }}
   {%- endif %}
        - {% for label in item.labels -%}
   {%- if label.startswith('Manager::') -%}
          .. gitlabuser::
             :username: {{ label | replace('Manager::@', '') }}
   {% endif -%}
   {%- endfor %}
        - {% for label in item.labels -%}
   {%- if label.startswith('Champion::') -%}
          .. gitlabuser::
             :username: {{ label | replace('Champion::@', '') }}
   {% endif -%}
   {%- endfor %}
   {%- endif -%}
   {%- endfor -%}
   {%- if not issues %}
      * -
        -
        -
   {% endif -%}


Other projects can be found in lsf-core repository.

***********************
Administrative projects
***********************

The following projects are administrative projects running vertically across
Libre Space Foundation:

.. gitlabissues::
   :project: librespacefoundation/lsf-core
   :labels: Ongoing,Administrative
   :sections_regex: ^\#{2}\s*(.*?)(?:\#{2})?\s*\n+(.*?)\n*(?=\n\#{2}|\Z)

   .. list-table::
      :header-rows: 1

      * - Project
        - Manager
        - Champion
   {%- for item in issues | rejectattr('confidential') -%}
   {%- if item.content['Public link'] is defined and item.content['Public link'].startswith('http') %}
      * - `{{ item.title }} <{{ item.content['Public link'].splitlines() | first }}>`_
   {%- else %}
      * - {{ item.title }}
   {%- endif %}
        - {% for label in item.labels -%}
   {%- if label.startswith('Manager::') -%}
          .. gitlabuser::
             :username: {{ label | replace('Manager::@', '') }}
   {% endif -%}
   {%- endfor %}
        - {% for label in item.labels -%}
   {%- if label.startswith('Champion::') -%}
          .. gitlabuser::
             :username: {{ label | replace('Champion::@', '') }}
   {% endif -%}
   {%- endfor %}
   {%- endfor -%}
   {%- if not issues %}
      * -
        -
        -
   {% endif -%}


**************************
Previously active projects
**************************

The following major projects have previously been in active development:

+---------------+---------------------+------------------+
| Project       | Project Manager     | Project Champion |
+===============+=====================+==================+
| `OpenSatCom`_ | `Pierros Papadeas`_ | N/A              |
+---------------+---------------------+------------------+
| UPSat_        | `Pierros Papadeas`_ | N/A              |
+---------------+---------------------+------------------+

.. _OpenSatCom: https://opensatcom.org
.. _UPSat: https://gitlab.com/groups/librespacefoundation/satnogs/_client-and-network/-/shared

************
Contributors
************

Libre Space Foundation is an active and welcoming community around open source
space projects.
We welcome all contributors in our projects and repositories.
We recognize those contributors that have been making considerable
contributions in our projects by inviting them in our "LSF Core Contributors"
group.
Learn more about :doc:`../people/core-contributors`.
