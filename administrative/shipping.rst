########
Shipping
########

Many times we need to ship items around the world. The followng are some services we use and have accounts setup:

.. warning::
    Before using any vendors below, please check prices using a site like `ShipLemon <https://www.shiplemon.com/>`_.

+-------------------------------------+--------------------+----------------------+---------+-------------------------------------+
|           Shipping Vendor           |        Type        |       Username       | Access  |                Notes                |
+=====================================+====================+======================+=========+=====================================+
| `Fedex <https://www.fedex.com>`_    | Business Account   | shipping@libre.space | Pierros | Business account, Details completed |
+-------------------------------------+--------------------+----------------------+---------+-------------------------------------+
| `DHL <https://mydhl.express.dhl>`_  | DHL+ Account       | shipping@libre.space | Pierros | Business account, Details completed |
+-------------------------------------+--------------------+----------------------+---------+-------------------------------------+
| `ACS <https://www.acscourier.net>`_ | Account for Greece | shipping@libre.space | Pierros | Business account, Details completed |
+-------------------------------------+--------------------+----------------------+---------+-------------------------------------+


.. note::
   Get familiar with `Incoterms <https://en.wikipedia.org/wiki/Incoterms>`_ since those are really usuful terms when arranging shipping.

.. note::
   For our HQ details (company numbers, address and info) see :doc:`../administrative/entity`.
