##########
Accounting
##########

Libre Space Foundation AMKE (the legal entity) employes the double-entry bookeeping method for accounting and financial managemnet purposes.

A dedicated ERP system (`erp.libre.space <https://erp.libre.space>`_) is deployed to meet the Accounting, Procurement and Sales functions needed.

********
Payments
********

Besides the regular :doc:`../administrative/procurement` process that applies to major items (~500+ EUR), we have the following three cases:

1. You have an LSF regular debit card (you are Manthos, Pierros or DimitrisP)
    a. You make a purchase
    b. You send the invoice or receipt directly to bills@libre.space
2. You have an LSF Curve card (linked to regular debit cards)
    a. You make a purchase
    b. You send the invoice or receipt directly to bills@libre.space
3. You pay with your own money (reimburshment needed)
    a. You make a purchase
    b. You scan an upload the invoice or receipt to bank@libre.space (this creates an issue in our gitlab repo)
    c. A member of the financial team, progresses your issue (you get email updates!)
    d. Once the payment has been made, the financial team member makes sure to upload the receipt to ERP and notify accoutning appropriately.

.. warning::
    Regular procurement rules still apply in minor items i.e. preffered vendors, invoices etc. See :doc:`../administrative/procurement` for details.
