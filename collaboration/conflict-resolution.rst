###################
Conflict Resolution
###################

.. contents::
   :local:
   :backlinks: none

********
Overview
********

Working with other people may sometimes lead to disagreements.
These disagreements may start as engineering decision disputes but can evolve
to personal or emotional conflicts.
In most cases, such disagreement are extinguished at the spot during day-to-day
communication and do not lead to major conflicts.
This section describes a formal process followed in Libre Space Foundation to
negotiate and resolve conflicts between individuals.
The process is mainly based on private meetings between the individuals and a
defined course of action which should resolve any conflict as early as
possible.

***************
Private Meeting
***************

This is the first action that should be attempted to resolve a conflict.
The offended party is defined as the individual who recognizes first that a
conflict exists with another individual.
The offending party is defined as the other individual in the conflict.
The offended party communicates to the offending party the will to invoke the
current process.
Both parties hold a private meeting to discuss the issue at hand on a mutually
agreed time and place.
The parties discuss and attempt to reach an agreement.
If an agreement is not reached and the conflict still continues, then they move
to the next action.

********************
Involving a Mediator
********************

This is the second action that should be attempted to resolve a conflict, if
the previous action fails.
The parties agree on a mutually accepted mediator, invited as a facilitator to
their discussions.
The offended, offending parties and the mediator hold a private meeting to
discuss the issue at hand on a mutually agreed time and place.
The parties discuss and attempt to reach an agreement, with the help of the
mediator.
The mediator shall use their skill to improve communication between the parties
and must be very careful not to express opinion or take the side of any party.
If an agreement is not reached and the conflict still continues, then they move
to the next action.

*************
Seek Guidance
*************

This is the last action that should be attempted to resolve a conflict, if the
previous action fails.
The parties seek guidance and advice on how to reach an agreement from Libre
Space Foundation Board.
Libre Space Foundation Board is the body responsible for preserving cultural
integrity and leading transformations within the organization.
